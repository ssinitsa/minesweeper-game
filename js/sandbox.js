function generateMineField(rowNumber, cellNumber) {
  const mine = 9;
  const emptyCell = 0;
  const mineNumber = 3;

  const field = [
    [0, 9, 0],
    [9, 0, 0],
    [0, 0, 9]
  ];

  // fill the field with numbers
  for (let rowIndex in field) {
    for (let cellIndex in field[rowIndex]) {
      rowIndex = +rowIndex;
      cellIndex = +cellIndex;

      if (field[rowIndex][cellIndex] === emptyCell) {
        
        let minesCount = 0;
        for (let i = rowIndex - 1; i <= rowIndex + 1 ; i++) {
          for (let k = cellIndex - 1; k <= cellIndex + 1; k++) {
            if (field[i] && field[i][k] === mine) {
              minesCount++;
            }
          }
        }
  
        field[rowIndex][cellIndex] = minesCount;
      }
    }
  }

  return field;
}

console.log('-------------');

let mineField = generateMineField(3, 3);
for (let row of mineField) console.log(row);



