function createMineField(rowsQuantity, cellsQuantity, firstCell) {
  const minesQuantity = Math.floor((rowsQuantity * cellsQuantity) / 6);

  const field = createEmptyField(rowsQuantity, cellsQuantity);
  plantMines(field, minesQuantity, firstCell);
  fillWithNumbers(field);

  return field;
}

function randomInteger(min, max) {
  return Math.floor(min + Math.random() * (max + 1 - min));
}

function createEmptyField(rowsQuantity, cellsQuantity) {
  const field = [];

  for (let i = 0; i < rowsQuantity; i++) {
    const row = [];
    for (let k = 0; k < cellsQuantity; k++) {
      row.push(0);
    }
    field.push(row);
  }

  return field;
}

function plantMines(field, minesQuantity, firstCell) {
  const rowsQuantity = field.length;
  const cellsQuantity = field[0].length;
  const [rowIndex, cellIndex] = firstCell;

  let minesPlanted = 0;

  while(minesPlanted < minesQuantity) {
    const randRow = randomInteger(0, rowsQuantity - 1);
    const randCell = randomInteger(0, cellsQuantity - 1);
    
    if (field[randRow][randCell] === 0 &&
        randRow !== rowIndex &&
        randCell !== cellIndex) {
      field[randRow][randCell] = 9;
      minesPlanted++;
    }
  }

  return field;
}

function fillWithNumbers(field) {
  for (let rowIndex in field) {
    for (let cellIndex in field[rowIndex]) {
      rowIndex = +rowIndex;
      cellIndex = +cellIndex;

      if (field[rowIndex][cellIndex] === 0) {
        const minesAround = calculateMinesAroundCell(field, rowIndex, cellIndex);
        field[rowIndex][cellIndex] = minesAround;
      }
    }
  }
}

function calculateMinesAroundCell(field, rowIndex, cellIndex) {
  let minesCount = 0;

  for (let row = rowIndex - 1; row <= rowIndex + 1; row++) {
    for (let cell = cellIndex - 1; cell <= cellIndex + 1; cell++) {
      if (field[row] && field[row][cell] === 9) {
        minesCount++;
      }
    }
  }

  return minesCount;
}

function createGameField(rowsQuantity, cellsQuantity) {
  const field = document.createElement('table');

  for (let i = 0; i < rowsQuantity; i++) {
    const row = document.createElement('tr');
    for (let j = 0; j < cellsQuantity; j++) {
      const cell = document.createElement('td');

      cell.dataset.row = i;
      cell.dataset.cell = j;
      cell.classList.add('cell', 'closed');

      row.appendChild(cell);
    }
    field.appendChild(row);
  }

  return field;
}

function clickHandler({target}) {
  if (target.tagName !== 'TD') return;
  const rowIndex  = +target.dataset.row;
  const cellIndex = +target.dataset.cell;
  
  if (clicksCount === 0) {
    mineField = createMineField(rows, cells, [rowIndex, cellIndex]);
  }

  if (mineField[rowIndex][cellIndex] === 9) {
    revealMines(mineField);
  }
  
  clicksCount++;
  openCell(rowIndex, cellIndex);
}

function rightClickHandler({target}) {
  event.preventDefault();
  if (target.classList.contains('opened')) return;

  if (target.classList.contains('flag')) {
    target.classList.remove('flag');
    target.classList.add('question');
  } else if (target.classList.contains('question')) {
    target.classList.remove('question');
  } else {
    target.classList.add('flag');
  }
}

function openCell(rowIndex, cellIndex) {
  const cell = board.children[0].children[rowIndex].children[cellIndex];

  if (cell.classList.contains('opened')) return;
  
  let cellContent = mineField[rowIndex][cellIndex];
  let cellClass = `num-${cellContent}`;
  if (cellContent === 9) cellClass = 'mine';
  
  cell.classList.remove('flag', 'question');
  cell.classList.add('opened', cellClass);

  if (!cellContent) {
    for (let row = rowIndex - 1; row <= rowIndex + 1; row++) {
      for (let cell = cellIndex - 1; cell <= cellIndex + 1; cell++) {
        if (mineField[row] && mineField[row][cell] !== undefined) {
          setTimeout(openCell, randomInteger(30, 130), row, cell);
        }
      }
    }
  }
}

function revealMines(mineField) {
  const mines = getMines(mineField);

  for (let mine of mines) {
    setTimeout(openCell, randomInteger(600, 1200), mine[0], mine[1]);
  }

  setTimeout(disactivateBoard, 1200);
}

function disactivateBoard() {
  const cells = document.getElementsByClassName('cell');
  
  for (let cell of cells) {
    if (!cell.classList.contains('opened')) {
      setTimeout(disactivateCell, randomInteger(0, 600), cell);
    }
  }
}

function disactivateCell(cell) {
  cell.classList.add('disactivated');
}

function getMines(mineField) {
  const mines = [];

  for (let rowIndex in mineField) {
    for (let cellIndex in mineField[rowIndex]) {
      rowIndex = +rowIndex;
      cellIndex = +cellIndex;

      if (mineField[rowIndex][cellIndex] === 9) {
        mines.push([rowIndex, cellIndex]);
      }
    }
  }

  return mines;
}

function preventMenu(event) {
  event.preventDefault();
}