const rows = 22;
const cells = 22;
const field = createGameField(rows, cells);

let mineField = null;
let clicksCount = 0;

board.appendChild(field);

board.addEventListener('click', clickHandler);
board.addEventListener('contextmenu', rightClickHandler);